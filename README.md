# MiTE-SysMonitor

# How To Install MiTE SysMonitor Tools To Monitor My Server.
 ###### Method 1 :

```bash
curl https://gitlab.com/DevOpsExpert/mite-sysmonitor/raw/master/install.sh | bash -;
cd ${HOME}/MiTE-SysMonitor; ./SysMonitor
```
We can pass name and email `
./SysMonitor "<your_name>:<your_email_id>"`
###### Method 2 :

MiTE SysMonitor Agent can be install by cloning this project.
```bash
git clone https://gitlab.com/DevOpsExpert/mite-sysmonitor.git;
cd mite-sysmonitor; chmod +x SysMonitor; ./SysMonitor

```

**Note** : You should pass your name and your email. This tool will send server health alert on your email.

###### Configure Emails to Get Server Alerts/Notification. if threshost limit cross.

This tool allow you to send email at multiple email id's. We just need to register with user name and single valid email. After that we can update emails or add new email id in the list.
We have a variable for username and email id in `SysMonitor.conf`. You can Add more email id in this list. you will notified if any issue found on server.



**Thanks**
  </br> __Developed by__ : Harry </br>
  **Email ID** : HarryTheDevOpsGuy@gmail.com </br>
  __Skype ID__ : HarryTheITExpert
