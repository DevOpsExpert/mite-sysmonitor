#!/usr/bin/env bash

GIT_BIN=$(command -v git)
GIT_CLONE_PATH="${HOME}/MiTE-SysMonitor"
GIT_ARGS="--depth=1 -q --branch master"

if [[ -z ${GIT_BIN} ]]; then
  echo "Installing git"
  sudo apt-get install git -y
fi

GIT_BIN=$(command -v git)

if [[ ! -z ${GIT_BIN} && ! -d ${GIT_CLONE_PATH} ]]; then
  git clone ${GIT_ARGS} https://gitlab.com/DevOpsExpert/mite-sysmonitor.git ${GIT_CLONE_PATH}
else
  echo "already exists"
fi

cd ${GIT_CLONE_PATH}/
sudo chmod +x SysMonitor
